# BBB voice conference access

Demo scripts to test unauthorized access to BBB voice conferences. 

- `demo1.html` + `dist/demo-2.js` - "normal" call through freeswitch
  - join some conference on a BBB server, note your sessionToken
  - change  `const webSocketServer = "wss://SERVER/ws?sessionToken=XXXXX";` to your server name and session token
  - change `const target = "sip:12345@SERVER";` to another sip address
  - open `demo1.html` in browser, try to call
  - w/o fixes: you can call any number, join any conference, maybe
    meet interesting people
- `demo-listen-only.html` - listen-only call through kurento
  - join some conference on a BBB server, note your sessionToken
  - replace "SERVER" with your server name (script src at the top of
    the file, audio.src and websocket url further down)
  - extract turn/stun config from a valid session and add it in line 97 and following
  - change number to call and sessionToken in call to `attack_listen_only()` (line 171)
  - open `demo-listen-only.html` in browser, click run to try to connect
